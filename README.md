```
DROP TABLE IF EXISTS information CASCADE;

CREATE TABLE information AS
select acc_aux."A_TOD", acc_aux."A_DOW", veh_aux."YEAR", veh_aux."ST_CASE", veh_aux."VEH_NO", veh_aux."A_BODY", veh_aux."A_IMP1", 
veh_aux."A_VROLL", veh_aux."A_MOD_YR", vehicle."VE_FORMS", vehicle."HARM_EV", vehicle."MAKE", vehicle."MODEL",
vehicle."MAK_MOD", vehicle."BODY_TYP", vehicle."MOD_YEAR", vehicle."GVWR", vehicle."DEATHS"
from acc_aux 
join veh_aux
on acc_aux."ST_CASE" = veh_aux."ST_CASE" 
join vehicle
on veh_aux."ST_CASE" = vehicle."ST_CASE"
where acc_aux."ST_CASE" between 480000 and 489999;

select * from information;

--check the relationship before how old of the car and the accident, result no causality relationship
select count(information."A_TOD"), information."A_MOD_YR" from information
group by information."A_MOD_YR"
order by information."A_MOD_YR" desc

---------------------------------------
DROP TABLE IF EXISTS vehicle_type CASCADE;

CREATE TABLE vehicle_type AS
select "A_BODY", count("A_BODY") from information
group by "A_BODY";

select * from vehicle_type;

----------------------------------------
DROP TABLE IF EXISTS initial_impact_point CASCADE;

CREATE TABLE initial_impact_point AS
select "A_IMP1", count("A_IMP1") from information
group by "A_IMP1";

select * from initial_impact_point;

---------------------------------------
DROP TABLE IF EXISTS roll_over CASCADE;

CREATE TABLE roll_over AS
select "A_VROLL", count("A_VROLL") from information
group by "A_VROLL";

select * from roll_over;
--------------------------------------

DROP TABLE IF EXISTS manufacture CASCADE;

CREATE TABLE manufacture AS
select "MAKE", count("MAKE") from information
group by "MAKE";

select * from manufacture;
--------------------------------------
DROP TABLE IF EXISTS first_harmful_event CASCADE;

CREATE TABLE first_harmful_event AS
select "HARM_EV", count("HARM_EV") from information
group by "HARM_EV";

select * from first_harmful_event;
--------------------------------------
DROP TABLE IF EXISTS model CASCADE;

CREATE TABLE model AS
select "MODEL", count("MODEL") from information
group by "MODEL";

select * from model;
--------------------------------------
DROP TABLE IF EXISTS deaths CASCADE;

CREATE TABLE deaths AS
select "DEATHS", count("DEATHS") from information
group by "DEATHS";

select * from deaths;
--------------------------------------
DROP TABLE IF EXISTS t_r CASCADE;

CREATE TABLE t_r AS

select type, r, sub*1.0/total as rato
from
(
	select information."A_BODY" as type, "A_VROLL" as r, count(information."A_BODY") as sub, 
(select vehicle_type.count from vehicle_type where vehicle_type."A_BODY" = information."A_BODY")as total from information
group by information."A_BODY", "A_VROLL") as t; 
----------------------------------------
DROP TABLE IF EXISTS ma_r CASCADE;

CREATE TABLE ma_r AS

select ma, r, sub*1.0/total as ratio
from
(
	select information."MAKE" as ma, "A_VROLL" as r, count(information."MAKE") as sub, 
(select manufacture.count from manufacture where manufacture."MAKE" = information."MAKE")as total from information
group by information."MAKE", "A_VROLL") as t; 

----------------------------------------
DROP TABLE IF EXISTS r_h CASCADE;

CREATE TABLE r_h AS

select r, h, sub*1.0/total as ratio
from
(
	select information."A_VROLL" as r, information."HARM_EV" as h, count(information."HARM_EV") as sub, 
(select roll_over.count from roll_over where roll_over."A_VROLL" = information."A_VROLL")as total from information
group by information."HARM_EV", information."A_VROLL") as t; 
------------------------------------------
DROP TABLE IF EXISTS h_f CASCADE;

CREATE TABLE h_f AS

select h, f, sub*1.0/total as rato
from
(
	select information."HARM_EV" as h, information."DEATHS" as f, count(information."HARM_EV") as sub, 
(select first_harmful_event.count from first_harmful_event where first_harmful_event."HARM_EV" = information."HARM_EV")as total from information
group by information."HARM_EV", information."DEATHS") as t; 
------------------------------------------

//Neo4j
LOAD CSV WITH HEADERS FROM "file:/Users/sophia/Downloads/information.csv" AS row
CREATE (:Information {A_TOD: row.A_TOD, A_DOW: row.A_DOW,	YEAR: row.YEAR,	ST_CASE: row.ST_CASE,	VEH_NO: row.VEH_NO,	A_BODY: row.A_BODY,	A_IMP1: row.A_IMP1,	A_VROLL: row.A_VROLL,	A_MOD_YR: row.A_MOD_YR,	VE_FORMS: row.VE_FORMS,	HARM_EV:row.HARM_EV,	MAKE: row.MAKE,	MODEL: row.MODEL,	MAK_MOD:row.MAK_MOD,	BODY_TYP: row.BODY_TYP,	MOD_YEAR: row.MOD_YEAR,	GVWR: row.GVWR,	DEATHS:row.DEATHS});

LOAD CSV WITH HEADERS FROM "file:/Users/sophia/Downloads/vehicle_type.csv" AS row
CREATE (:Vehicle_type {A_BODY: row.A_BODY, count: row.count});

LOAD CSV WITH HEADERS FROM "file:/Users/sophia/Downloads/deaths.csv" AS row
CREATE (:Fatal {DEATHS: row.DEATHS, count: row.count});

LOAD CSV WITH HEADERS FROM "file:/Users/sophia/Downloads/first_harmful_event.csv" AS row
CREATE (:First_harmful_event {HARM_EV: row.HARM_EV, count: row.count});

LOAD CSV WITH HEADERS FROM "file:/Users/sophia/Downloads/initial_impact_point.csv" AS row
CREATE (:Initial_impact_point {A_IMP1: row.A_IMP1, count: row.count});

LOAD CSV WITH HEADERS FROM "file:/Users/sophia/Downloads/manufacture.csv" AS row
CREATE (:Manufacture {MAKE: row.MAKE, count: row.count});

LOAD CSV WITH HEADERS FROM "file:/Users/sophia/Downloads/model.csv" AS row
CREATE (:M_type {MODEL: row.MODEL, count: row.count});

LOAD CSV WITH HEADERS FROM "file:/Users/sophia/Downloads/roll_over.csv" AS row
CREATE (:Roll_over {A_VROLL: row.A_VROLL, count: row.count});

LOAD CSV WITH HEADERS FROM "file:/Users/sophia/Downloads/t_r.csv" AS row
CREATE (:T_r {type: row.type, r: row.r, ratio: row.rato});

LOAD CSV WITH HEADERS FROM "file:/Users/sophia/Downloads/ma_r.csv" AS row
CREATE (:Ma_r {ma: row.ma, r: row.r, ratio: row.ratio});

LOAD CSV WITH HEADERS FROM "file:/Users/sophia/Downloads/r_h.csv" AS row
CREATE (:R_h {r: row.r, h: row.h, ratio: row.ratio});

LOAD CSV WITH HEADERS FROM "file:/Users/sophia/Downloads/h_f.csv" AS row
CREATE (:H_f {h: row.h, f: row.f, ratio: row.rato});

MATCH(n:T_r) 
WITH n.type AS t, n.r AS ro, n.ratio AS ra //vehicle_type
MATCH(a:Vehicle_type),(b:Roll_over)
WHERE a.A_BODY = t AND b.A_VROLL = ro CREATE (a)-[r:TO{from:t, to:ro, ratio:ra}]->(b) RETURN a,b;

MATCH(n:Ma_r) 
WITH n.ma AS ma, n.r AS ro, n.ratio AS ra //vehicle_type
MATCH(a:Manufacture),(b:Roll_over)
WHERE a.MAKE = ma AND b.A_VROLL = ro CREATE (a)-[r:TO{from:ma, to:ro, ratio:ra}]->(b) RETURN a,b;

MATCH(n:R_h) 
WITH n.r AS ro, n.h AS ha, n.ratio AS ra //vehicle_type
MATCH(a:Roll_over),(b:First_harmful_event)
WHERE a.A_VROLL = ro AND b.HARM_EV = ha CREATE (a)-[r:TO{from:ro, to:ha, ratio:ra}]->(b) RETURN a,b;

MATCH(n:H_f) 
WITH n.h AS ha, n.f AS fa, n.ratio AS ra //vehicle_type
MATCH(a:First_harmful_event),(b:Fatal)
WHERE a.HARM_EV = ha AND b.DEATHS = fa CREATE (a)-[r:TO{from:ha, to:fa, ratio:ra}]->(b) RETURN a,b;

```